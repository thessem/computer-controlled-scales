/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          Text to Speech Manager
    Author(s):      James Littlejohn

============================================================================= */
#include <xc.h>
#include <GenericTypeDefs.h>
#include <stdint.h>
#include "hardware_info.h"
#include "spi/spi.h"

#include "tts/tts.h"

// TTS Commands. I'm not going to comment these, because I'd just be wasting
// my time doing a poor job summarising the data sheet.
#define TTS_PWUP        0x02
#define TTS_RDST        0x04
#define TTS_RINT        0x06
#define TTS_ENTER_RRSM  0x0C
#define TTS_RST         0x10
#define TTS_RVER        0x12
#define TTS_SCLS        0x14
#define CFG_CLK_24M     0x00
#define TTS_PWDN        0x40
#define TTS_EXIT_RRSM   0x41
#define TTS_PAUS        0x49
#define TTS_RES         0x4A
#define TTS_STOP        0x4B
#define TTS_FIN         0x4C
#define TTS_FINW        0x4D
#define TTS_SCOM        0x4E
#define TTS_SCOD        0x4F
#define TTS_SAUD        0x50
#define TTS_SVOL        0x51
#define TTS_SSPD        0x52
#define TTS_VLUP        0x53
#define TTS_VLDN        0x54
#define TTS_SPUP        0x55
#define TTS_SPDN        0x56
#define TTS_IDLE        0x57
#define TTS_SPTC        0x77
#define TTS_CONV        0x81
#define TTS_ABBR_DEL    0x83
#define TTS_ABBR_ADD    0xAF
#define TTS_RREG        0xC0
#define TTS_ABBR_MEM    0xC7
#define TTS_ABBR_NUM    0xC8
#define TTS_ABBR_RD     0xC9
#define TTS_EOT         0x1A

static BOOL initialised = FALSE;

void tts_SendCommand(const uint8_t command1, const uint8_t command2)
{
    spi_ChangeChannel(TTS);
    spi_TrasmitByte(command1);
    spi_TrasmitByte(command2);
    spi_ChangeChannel(NONE);
}

// TTS always excpects commands in groups of two. A single command is padded
void tts_SendSingleCommand(const uint8_t command)
{
    tts_SendCommand(command, 0x00);
}

void tts_Init()
{
    if (initialised)
        return;
    
    spi_Init();

    TTS_RESET_TRIS = 0;
    TTS_RB_TRIS = 1;

    // Toggle Reset Signal
    TTS_RESET = 1;
    for (uint8_t ii = 0; ii < 10; ii++, __delay_ms(10));
    TTS_RESET = 0;

    // Power up the board
    tts_SendCommand(TTS_SCLS, CFG_CLK_24M);
    // I think there needs to be a delay after this
    for (uint8_t ii = 0; ii < 10; ii++, __delay_ms(10));
    tts_SendSingleCommand(TTS_PWUP);
    // There definitely needs to be a delay after this
    for (uint8_t ii = 0; ii < 10; ii++, __delay_ms(10));

    // Now we just walk through the startup procedure in the datasheet
    // The parameters to the commands that take data are defined in the 
    // datasheet.
    tts_SendCommand(TTS_SCOM, 0x00); // Com Interrupt style
    tts_SendCommand(TTS_SCOD, 0x01); // Codec
    tts_SendCommand(TTS_SAUD, 0x43); // Audio
    tts_SendCommand(TTS_SVOL, 0x00); // Volume
    tts_SendCommand(TTS_SSPD, 0x02); // Speed
    tts_SendCommand(TTS_SPTC, 0x05); // Pitch

    initialised = TRUE;
}

void tts_Speak(const char* text)
{
    // Converstion command
    spi_ChangeChannel(TTS);
    spi_TrasmitByte(TTS_CONV);
    spi_TrasmitByte(0x01);
    while (*text != '\0')
    {
        spi_TrasmitByte(*text++);
    }
    // Send EOT
    spi_TrasmitByte(TTS_EOT);
    spi_ChangeChannel(NONE);
}