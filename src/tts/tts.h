#ifndef __TTS_TTS_H__
#define __TTS_TTS_H__
/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          Text to Speech Manager
    Author(s):      James Littlejohn

============================================================================= */
    
/** 
 *  @ingroup HARDWARE
 *  @defgroup TTS tts.h : Text to Speech Manager
 *
 *  A manager for Text to Speech hardware.
 *
 *  Files: tts.h & tts.c
 *
 *  This module sets up and controls the Text to Speech hardware. Currently
 *  lacks any advanced features or flow control.
 *  
 *  @{
 */

/**
 *  Initialisation function. Resets and starts up the TTS hardware.
 */
extern void tts_Init(void);

/**
 *  Causes the TTS hardware to speak a string of text.
 *
 *  @param[out] text  Nuil terminated string of text to say outloud
 */
extern void tts_Speak(const char* text);

/**
  *  @}
  */
#endif
