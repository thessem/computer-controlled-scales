/* =============================================================================
    
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Title:          Button Manager
    Author(s):      James Littlejohn

============================================================================= */

#include <xc.h>
#include <GenericTypeDefs.h>
#include <stdint.h>
#include "hardware_info.h"

#include "buttons/buttons.h"

// Init function pointers to null.
void (*buttons_ModePressed)(void) = NULL;
void (*buttons_UnitPressed)(void) = NULL;
void (*buttons_TarePressed)(void) = NULL;
void (*buttons_UpPressed)(void) = NULL;
void (*buttons_DownPressed)(void) = NULL;
void (*buttons_FactoryMode)(void) = NULL;
void (*buttons_TTSReadyBusy)(void) = NULL;

void buttons_Init(void)
{
    // Set buttons as inputs
    BUTTON_MODE_INTERRUPT_TRIS = 1;
    BUTTON_MODE_TRIS = 1;
    BUTTON_UNIT_TRIS = 1;
    BUTTON_TARE_TRIS = 1;
    BUTTON_UP_TRIS = 1;
    BUTTON_DOWN_TRIS = 1;
    BUTTON_TTS_RB_TRIS = 1;

    // Check for Factory Mode shortcut (only avaliable on boot)
    if (BUTTON_UNIT == 1 && BUTTON_TARE == 1)
    {
        if (buttons_FactoryMode != NULL)
        (*buttons_FactoryMode)();
    }

    // Turn on interrupt for interrupt line
    INTCONbits.RBIE = 1;
    INTCONbits.RBIF = 0;
}

void buttons_ProcessTasks(void)
{
    if (INTCONbits.RBIF != 1)
        return;

    // We need to read PORTB so this interrupt doesn't always fire. I'm just
    // following the datasheet on this one
    uint8_t data = PORTB;

    // Figure out which buttons are pressed, and call the appropriate functions
    if (buttons_ModePressed != NULL && BUTTON_MODE == 1)
        (*buttons_ModePressed)();

    if (buttons_UnitPressed != NULL && BUTTON_UNIT == 1)
        (*buttons_UnitPressed)();

    if (buttons_TarePressed != NULL && BUTTON_TARE == 1)
        (*buttons_TarePressed)();

    if (buttons_UpPressed != NULL && BUTTON_UP == 1)
        (*buttons_UpPressed)();

    if (buttons_DownPressed != NULL && BUTTON_DOWN == 1)
        (*buttons_DownPressed)();
    
    if (buttons_TTSReadyBusy != NULL && BUTTON_TTS_RB_TRIS == 1)
        (*buttons_TTSReadyBusy)();
    
    // I don't know whether we need to reset the flag, but lets be safe.
    INTCONbits.RBIF = 0;
}
