#ifndef __BUTTONS_BUTTONS_H__
#define __BUTTONS_BUTTONS_H__
/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          Button Manager
    Author(s):      James Littlejohn

============================================================================= */

/** 
 *  @ingroup HARDWARE
 *  @defgroup BUTTONS buttons.h : Button Manager
 *
 *  A manager for the front panel buttons.
 *
 *  Files: buttons.h & buttons.c
 *
 *  The front panel buttons are connected to the PIC using straight digital 
 *  inputs, along with an interrupt line formed by ORing all the buttons 
 *  together. The interrupt line is connected to a interrupt on change input,
 *  which, when triggered, will cause the button manager to check each of its
 *  inputs. It will then call the function pointer that corresponds with
 *  whichever button are active.
 *
 *  This module will also check if the button combination for factory mode is
 *  active when the device boots.
 *  
 *  @{
 */

/**
 *  Called when the Mode button is pressed.
 */
extern void (*buttons_ModePressed)(void);

/**
 *  Called when the Unit button is pressed.
 */
extern void (*buttons_UnitPressed)(void);

/**
 *  Called when the Tare button is pressed.
 */
extern void (*buttons_TarePressed)(void);

/**
 *  Called when the Up (+) button is pressed.
 */
extern void (*buttons_UpPressed)(void);

/**
 *  Called when the Down (-) button is pressed.
 */
extern void (*buttons_DownPressed)(void);

/**
 *  Called if the Factory mode combo is detected on startup.
 */
extern void (*buttons_FactoryMode)(void);

/**
 *  Called when the TTS Module transitions its R/B output.
 */
extern void (*buttons_TTSReadyBusy)(void);

/**
 *  Initialisation function. Sets up interrupt on PORTB change. Must be called
 *  before any other function in this module is called.
 */
extern void buttons_Init(void);

/**
 *  The interrupt function of this module. It needs to be called every time an
 *  interrupt is detected. If the interrupt is relevant to this module, it will
 *  deal with it at reset the appropriate flag.
 */
extern void buttons_ProcessTasks(void);

/**
  *  @}
  */
#endif
