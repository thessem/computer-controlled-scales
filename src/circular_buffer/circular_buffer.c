/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          Ciircular Buffer
    Author(s):      James Littlejohn

============================================================================= */
#include <string.h> // For memcpy

#include "circular_buffer/circular_buffer.h"

void circular_buffer_Init(CircularBuffer_t* const cb, const void* const buffer, 
                          const size_t capacity, const size_t elm_size) {
    cb->capacity  = capacity;
    cb->start = 0;
    cb->end   = 0;
    cb->buffer = buffer;
    cb->elem_size = elm_size;
}

BOOL circular_buffer_IsFull(const CircularBuffer_t* const cb) {
     // This inverts the most significant bit of start before comparison
    return cb->end == (cb->start ^ cb->capacity);
}

BOOL circular_buffer_IsEmpty(const CircularBuffer_t* const cb) {
    return cb->end == cb->start;
}

static size_t circular_buffer_Incr(const CircularBuffer_t* const cb, 
                                   const size_t p) {
     // start and end pointers incrementation is done modulo 2*size
    return (p + 1)&(2*cb->capacity-1);
}

void circular_buffer_Write(CircularBuffer_t* const cb, const void* const data) {
    memcpy((uint8_t*)cb->buffer + (cb->end&(cb->capacity-1))*cb->elem_size, 
           data, cb->elem_size);
    if (circular_buffer_IsFull(cb)) /* full, overwrite moves start pointer */
        cb->start = circular_buffer_Incr(cb, cb->start);
    cb->end = circular_buffer_Incr(cb, cb->end);
}

void circular_buffer_Read(CircularBuffer_t* const cb, void* const data) {
    memcpy(data, 
           (uint8_t*)cb->buffer + (cb->start&(cb->capacity-1))*cb->elem_size, 
           cb->elem_size);
    cb->start = circular_buffer_Incr(cb, cb->start);
}

size_t circular_buffer_WriteData(CircularBuffer_t* const cb, 
                                 const void* const data, 
                                 const size_t len)
{
    size_t ii;
    for (ii = 0; ii < len && ii < cb->capacity; ii++)
    {
        circular_buffer_Write(cb, (uint8_t*)data + ii*cb->elem_size);
    }
    return ii;
}

size_t circular_buffer_ReadData(CircularBuffer_t* const cb, 
                                void* const data, 
                                const size_t len)
{
    size_t ii;
    for (ii = 0; ii < len && !circular_buffer_IsEmpty(cb); ii++)
    {
        circular_buffer_Read(cb, (uint8_t*)data + ii*cb->elem_size);
    }
    return ii;
}
