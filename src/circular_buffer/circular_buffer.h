#ifndef __CIRCULAR_BUFFER_H__
#define __CIRCULAR_BUFFER_H__
/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          Circular Buffer
    Author(s):      James Littlejohn

============================================================================= */

/**
 *  @ingroup GENERAL
 *  @defgroup CIRCULAR_BUFFER circular_buffer.h : FIFO circular buffer
 *
 *  A data structure that uses a single, fixed-size buffer as if it were
 *  connected end-to-end (circular).
 *
 *  Files: circular_buffer.h & circular_buffer.c
 *
 *  A fixed-sized buffer is managed as a FIFO buffer with a "zero-copy" policy,
 *  i.e. data is not shifted (copied) when data is removed or added to the
 *  buffer. If more data is written to the buffer than there is space for, it is
 *  ignored/discarded; no buffer overflow vulnerability.
 *
 *  @see http://en.wikipedia.org/wiki/Circular_buffer
 *
 *  @{
 */

#include <xc.h>
#include <GenericTypeDefs.h>
#include <stdint.h>

/* Circular buffer object */
typedef struct {
    size_t capacity;    ///< Maximum number of elements
    size_t start;       ///< Index of oldest element
    size_t end;         ///< index at which to write new element
    const void* buffer; ///< Where the elements are stored
    size_t elem_size;   ///< Size of each element
} CircularBuffer_t;

static size_t circular_buffer_Incr(const CircularBuffer_t* const cb, 
								   const size_t p);

/**
 *  Initialize the circular buffer.
 *
 *  @param cb           Pointer to the circular buffer object
 *  @param buffer       Fixed-size data buffer
 *  @param capacity     Amount of elements stored within the buffer
 *  @param elm_size     Size of each element stored in this buffer
 */
extern void circular_buffer_Init(CircularBuffer_t* const cb, 
	                             const void* const buffer, 
	                             const size_t capacity, 
	                             const size_t elm_size);

/**
 *  See if the circular buffer is full.
 *
 *  @param cb       Pointer to the circular buffer object
 *
 *  @retval TRUE    Buffer is full
 *  @retval FALSE   Buffer is NOT full
 */
extern BOOL circular_buffer_IsFull(const CircularBuffer_t* const cb);

/**
 *  See if the circular buffer is empty.
 *
 *  @param cb      Pointer to the circular buffer object
 *
 *  @retval TRUE   Buffer is empty
 *  @retval FALSE  Buffer contains data
 */
extern BOOL circular_buffer_IsEmpty(const CircularBuffer_t* const cb);

/**
 *  Write (store) a byte in the circular buffer.
 *
 *  @param cb       Pointer to the circular buffer object
 *  @param data     Pointer to the byte to store in the circular buffer
 */
extern void circular_buffer_Write(CircularBuffer_t* const cb, 
	                              const void* const data);

/**
 *  Read (retrieve) a byte from the circular buffer.
 *
 *  @param cb       Pointer to the circular buffer object
 *  @param data     Pointer location where data will be stored
 */
extern void circular_buffer_Read(CircularBuffer_t* const cb, void* const data);

/**
 *  Write (store) data in the circular buffer.
 *
 *  @param cb       Pointer to the circular buffer object
 *  @param data     Pointer to array of data to be stored in the ring buffer
 *  @param len      Amount of data bytes to write
 */
extern size_t circular_buffer_WriteData(CircularBuffer_t* const cb, 
	                                    const void* const data, 
	                                    const size_t len);

/**
 *  Read (retrieve) data from the circular buffer.
 *
 *  @param cb       Pointer to the circular buffer object
 *  @param data     Pointer location where data will be stored
 *  @param len      Amount of data bytes to read
 */
extern size_t circular_buffer_ReadData(CircularBuffer_t* const cb, 
	                                   void* const data, 
	                                   const size_t len);

/**
  *  @}
  */
#endif