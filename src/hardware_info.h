#ifndef __HARDWARE_INFO_H__
#define __HARDWARE_INFO_H__
/* =============================================================================
    
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Title:          Hardware Info
    Author(s):      James Littlejohn

============================================================================= */

#define _XTAL_FREQ 10000000

#define UART_CLK_TRIS TRISCbits.TRISC6
#define UART_RX_TRIS  TRISCbits.TRISC7

#define SPI_SCK_TRIS TRISCbits.TRISC3
#define SPI_NSS_TRIS TRISCbits.TRISC4
#define SPI_SDO_TRIS TRISCbits.TRISC5

#define SPI_SELECT_LED_BAR       LATEbits.LATE1
#define SPI_SELECT_LED_BAR_TRIS  TRISEbits.TRISE1
#define SPI_SELECT_TTS           LATEbits.LATE2
#define SPI_SELECT_TTS_TRIS      TRISEbits.TRISE2

#define TTS_RB                   PORTAbits.PORTA1
#define TTS_RB_TRIS              TRISAbits.TRISA1
#define TTS_RESET                LATEbits.LATE0
#define TTS_RESET_TRIS           TRISEbits.TRISE0

#define LCD_PORT         LATD
#define LCD_PORT_TRIS    TRISD
#define LCD_RS_MASK      (1 << 4)
#define LCD_EN_MASK      (1 << 5)

#define BUTTON_MODE_INTERRUPT PORTBbits.RB4
#define BUTTON_MODE           PORTCbits.RC0
#define BUTTON_UNIT           PORTCbits.RC1
#define BUTTON_TARE           PORTBbits.RB0
#define BUTTON_UP             PORTCbits.RC2
#define BUTTON_DOWN           PORTBbits.RB1
#define BUTTON_TTS_RB         PORTBbits.RB2

#define BUTTON_MODE_INTERRUPT_TRIS TRISBbits.TRISB4
#define BUTTON_MODE_TRIS           TRISCbits.TRISC0
#define BUTTON_UNIT_TRIS           TRISCbits.TRISC1
#define BUTTON_TARE_TRIS           TRISBbits.TRISB0
#define BUTTON_UP_TRIS             TRISCbits.TRISC2
#define BUTTON_DOWN_TRIS           TRISBbits.TRISB1
#define BUTTON_TTS_RB_TRIS         TRISBbits.TRISB2

#endif
