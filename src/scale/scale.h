#ifndef __SCALE_SCALE_H__
#define __SCALE_SCALE_H__
/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          Scale Manager
    Author(s):      James Littlejohn

============================================================================= */
    
/** 
 *  @ingroup HARDWARE
 *  @defgroup SCALE scale.h : Scale Manager
 *
 *  A manager for scale hardware.
 *
 *  Files: scale.h & scale.c
 *
 *  This module will setup periodic sampling of the A2D hardware, storing the
 *  result in an integer buffer (avoiding float for speed). When the weight is
 *  requested, this module will average all the samples in the buffer, and then
 *  convert this weight to the relevant unit.
 *  
 *  @{
 */

#include <stdint.h>

/**
 *  An enum to represent the various kind of weight measurements we can do.
 */
typedef enum 
{ RAW,         /**< Weight as returned by the the A2D, as a 10 bit integer */
  GRAMS,       /**< Weight in grams */
  OUNCES,      /**< Weight in ounces */
  COUNT,       /**< Weight in terms of last item counted */
  RAW_NO_TARE  /**< Raw weight without the tare being accounted for */
} Weight_t;

/**
 *  Initialisation function. Sets up timed sampling of A2D system.
 */
extern void scale_Init(void);

/**
 *  The interrupt function of this module. It needs to be called every time an
 *  interrupt is detected. If the interrupt is relevant to this module, it will
 *  deal with it at reset the appropriate flag.
 */
extern void scale_ProcessTasks(void);

/**
 *  Tare the scales with the weight currently in the scale.
 */
extern void scale_Tare(void);

/**
 *  Sets up the count function. It will measure the pan, and using the amount of
 *  items passed to it, will calculate the weight of a single item.
 *
 *  @param items  The number of items currently in the pan
 */
extern void scale_SetCount(uint8_t items);

/**
 *  Returns the current weight of the pan, using whatever weight type is
 *  specified.
 *
 *  @param weight_type  What sort of weight to send back
 *
 *  @return int24_t     The weight reading
 */
extern int24_t scale_GetWeight(Weight_t weight_type);

/**
 *  Saves the current basic calibration (Raw-To-Grams) into the PIC EEPROM.
 *
 *  @param gradient  The Raw-To-Grams calibration to be saved
 */
extern void scale_SaveCalibration(double gradient);

/**
  *  @}
  */
#endif
