/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          Scale Manager
    Author(s):      James Littlejohn

============================================================================= */
#include <xc.h>
#include <GenericTypeDefs.h>
#include <math.h>
#include <stdint.h>
#include "hardware_info.h"

#include "scale/scale.h"
#include "circular_buffer/circular_buffer.h"

#define SCALE_BUFFER_SIZE 32    // Size of the buffer for the scale

static CircularBuffer_t scale_circular_buffer;
static uint16_t         scale_buffer[SCALE_BUFFER_SIZE];

// These values are roughly known good values
static uint16_t tare = 84;
static double grams_conversion = (1/1.226);
static double ounces_conversion = (1/1.226)*0.035274;
static double count_conversion = 0;

void scale_LoadCalibration(void)
{
    // Unfortunately I couldn't get this one working
    /*
    double gradient;
    uint8_t* data = &gradient;

    for (uint8_t address = 0; address < sizeof(double); address++)
    {
        EEADR=address;
        EECON1bits.EEPGD= 0; // 0 = Access data EEPROM memory
        EECON1bits.CFGS = 0; // 0 = Access Flash program or DATA EEPROM memory
        EECON1bits.RD   = 1; // EEPROM Read
        *data = EEADR;
        data++;
    }

    grams_conversion = gradient;
    ounces_conversion = grams_conversion*0.035274;
    */
}

uint16_t scale_RawWeight()
{
    uint16_t data;
    uint32_t running_average = 0;

    // Wait until we have enough samples
    while (!circular_buffer_IsFull(&scale_circular_buffer)) { }
    // Now average and return them
    for (uint8_t ii = 0; ii < SCALE_BUFFER_SIZE; ii++)
    {
        circular_buffer_Read(&scale_circular_buffer, &data);
        running_average += data;
    }
    return (running_average/SCALE_BUFFER_SIZE);
}

void scale_Init(void)
{
    circular_buffer_Init(&scale_circular_buffer, scale_buffer, 
                         SCALE_BUFFER_SIZE, 
                         sizeof(scale_buffer)/SCALE_BUFFER_SIZE);

    // Set pins
    TRISAbits.TRISA2 = 1;
    TRISAbits.TRISA3 = 1;

    // Set up analog input
    ADCON0bits.ADCS = 0b11; // Internal oscillator
    ADCON1bits.ADFM = 1;     // Right justified
    ADCON1bits.PCFG = 0b1111;  // AN3 Vref+, AN2 Vref-, AN0 is single analog in
    ADCON0bits.CHS  = 0;     // AN0 is the channel we're looking at
    PIE1bits.ADIE = 1;       // Turn on the interrupt
    ADCON0bits.ADON = 1;     // Turn this sucker on

    // Set up the timer that triggers A2D
    CCP2CONbits.CCP2M = 0b1011; // Trigger special event
    //T3CONbits.RD16 = 1;       // 16 bit R/W
    T3CONbits.T3CCP2 = 1;     // Source for CCP2
    T3CONbits.T3CKPS = 0b11;  // 1:8 Prescaling
    T3CONbits.TMR3CS = 0;     // Fosc/4 clock

    // Set up timer for CCP2
    CCPR2 = 3125;             // 100 Hz!

    // Load calibration
    scale_LoadCalibration();

    // Start the timer
    TMR3 = 0;                // Reset timer
    T3CONbits.TMR3ON = 1;    // Start
}

void scale_ProcessTasks(void)
{
    if (PIR1bits.ADIF != 1)
        return;
    uint16_t result = ADRES;
    circular_buffer_Write(&scale_circular_buffer, &result);

    PIR1bits.ADIF = 0;
}

void scale_Tare()
{
    tare = scale_RawWeight();
}

void scale_SetCount(uint8_t items)
{
    count_conversion = (double)items/(double)scale_GetWeight(RAW);
}

int24_t scale_GetWeight(Weight_t weight_type)
{
    double raw_weight = scale_RawWeight() - tare;
    switch (weight_type)
    {
        case RAW_NO_TARE:
            return (int24_t)raw_weight + tare;
        case GRAMS:
            return (int24_t)(raw_weight*grams_conversion);
        case OUNCES:
            return (int24_t)(raw_weight*ounces_conversion);
        case COUNT:
            // We round this one
            return (int24_t)floor(raw_weight*count_conversion + 0.5);
        case RAW:
            // Fall through to default
        default:
            return (int24_t)(raw_weight);
    }
}

void scale_SaveCalibration(double gradient)
{
    // Unfortunately I couldn't get this one working
    /*
    uint8_t int_save;
    uint8_t address;
    uint8_t* data = &gradient;

    // Save interrupts
    int_save = INTCON;
    INTCON = 0;

    // Write gradient
    for (address = 0; address < sizeof(gradient); address++)
    {
        EEADR  = address;
        EEDATA = *data++;

        EECON1bits.EEPGD= 0; // 0 = Access data EEPROM memory
        EECON1bits.CFGS = 0; // 0 = Access Flash program or DATA EEPROM memory
        EECON1bits.WREN = 1; // enable writes to internal EEPROM

        EECON2=0x55;        // Required sequence for write to internal EEPROM
        EECON2=0xaa;        // Required sequence for write to internal EEPROM

        EECON1bits.WR=1;    // begin write to internal EEPROM

        while (PIR2bits.EEIF==0)//Wait till write operation complete
        {
            Nop();
        }

        EECON1bits.WREN=0; // Disable writes to EEPROM on write complete
        PIR2bits.EEIF=0; //Clear EEPROM write complete flag.
    }

    INTCON=int_save; //Now we can safely enable interrupts if previously used
    EECON1bits.WREN=0; // Disable writes to EEPROM on write complete
    PIR2bits.EEIF=0; //Clear EEPROM write complete flag.
    */
}