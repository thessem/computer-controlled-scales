/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          LED Bar Manager
    Author(s):      James Littlejohn

============================================================================= */
#include <xc.h>
#include <GenericTypeDefs.h>
#include <stdint.h>
#include "hardware_info.h"
#include "spi/spi.h"

#include "led_bar/led_bar.h"

void led_bar_Init(void)
{
    // initialise SPI
    spi_Init();
    // Turn them all off
    led_bar_Raw(0x00);
}

void led_bar_SingleLed(const uint8_t led_id)
{
    // It's your own fault if you put something bigger than 16 into this.
    // It'll safetly overflow, but be really confusing
    led_bar_Raw(1 << led_id);
}

void led_bar_BarGraph(const uint8_t top_led, const uint8_t bottom_led)
{
    if (top_led > 16 || bottom_led > 16)
        return;
    
    // All the LEDs up to top_led
    const uint16_t full_bar = ~(0xFFFF >> (top_led));
    // All the LEDS on up to bottom_led
    const uint16_t bottom_bar = ~(0xFFFF >> (bottom_led));
    // Output full_bar with bottom_bar taken out of it
    led_bar_Raw(full_bar ^ bottom_bar);
}

void led_bar_Raw(const uint16_t raw)
{
    // Seperate into two bits
    // Bits are getting inverted too, because that's how the LEDs
    // are wired
    spi_ChangeChannel(LED_BAR);
    spi_TrasmitByte(~(raw >> 8));
    spi_ChangeChannel(NONE);
    spi_ChangeChannel(LED_BAR);
    spi_TrasmitByte(~(raw & 0xFF));
    spi_ChangeChannel(NONE);
    spi_ChangeChannel(LED_BAR);
}
