#ifndef __LED_BAR_LED_BAR_H__
#define __LED_BAR_LED_BAR_H__
/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          LED Bar Manager
    Author(s):      James Littlejohn

============================================================================= */

/** 
 *  @ingroup HARDWARE
 *  @defgroup LED_BAR led_bar.h : LED Bar Manager
 *
 *  A manager for the bar of LEDs attached to the scales.
 *
 *  Files: led_bar.h & led_bar.c
 *
 *  This is a bar of LEDs, attached to the PIC by means of an SPI chip. We just
 *  write the binary pattern desired to the LED Bar hardware and away we go.
 *  
 *  @{
 */

#include <stdint.h>

/**
 *  Initialisation function. Really just blanks all the LEDs just in case one of
 *  them is on.
 */
extern void led_bar_Init(void);

/**
 *  The function will turn on a single LED.
 *
 *  @param led_id  Which LED to turn on
 */
extern void led_bar_SingleLed(const uint8_t led_id);

/**
 *  The function will create a bar graph pattern.
 *
 *  @param top_led     How far up the graph goes
 *  @param bottom_led  How many LEDs to turn off at the bottom of bar
 */
extern void led_bar_BarGraph(const uint8_t top_led, const uint8_t bottom_led);

/**
 *  Send data to the LED bar, will display the binary of sent data
 *
 *  @param raw  The data to send
 */
extern void led_bar_Raw(const uint16_t raw);

/**
  *  @}
  */
#endif
