/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          SPI Manager
    Author(s):      James Littlejohn

============================================================================= */
#include <xc.h>
#include <GenericTypeDefs.h>
#include <stdint.h>
#include "hardware_info.h"
#include "circular_buffer/circular_buffer.h"

#include "spi/spi.h"

#define SPI_RECEIVE_BUFFER_SIZE 4    // Amount of bytes to keep around in memory

static BOOL             initialised = FALSE;
static CircularBuffer_t spi_receive_circular_buffer;
static uint8_t          spi_receive_buffer[SPI_RECEIVE_BUFFER_SIZE];
static Channel_t        current_channel = NONE;

void spi_Init(void)
{
    if (initialised)
        return;
    
    circular_buffer_Init(&spi_receive_circular_buffer, spi_receive_buffer, 
                         SPI_RECEIVE_BUFFER_SIZE, 
                         sizeof(spi_receive_buffer)/SPI_RECEIVE_BUFFER_SIZE);

    // Set pins
    SPI_SDO_TRIS = 0;
    SPI_SCK_TRIS = 0;
    SPI_NSS_TRIS = 1;

    // Set slave selects
    SPI_SELECT_LED_BAR_TRIS = 0;
    SPI_SELECT_TTS_TRIS = 0;

    // Set all slaves high
    SPI_SELECT_LED_BAR = 1;
    SPI_SELECT_TTS = 1;

    // Set control register
    SSPCON1bits.SSPM = 0b0001; // SPI Master mode, Fosc/16
    SSPCON1bits.SSPEN = 1; // Configure pins as serial
    SSPCON1bits.CKP = 0; // Low clock idle
    SSPSTATbits.SMP = 0; // Sample at middle
    SSPSTATbits.CKE = ~SSPCON1bits.CKP; // Data on rising edge

    // Clear error registers
    SSPCON1bits.SSPOV = 0;
    SSPCON1bits.WCOL = 0;

    initialised = TRUE;
}

void spi_ChangeChannel(Channel_t channel)
{
    // Changing to the current channel shouldn't do anything
    if (channel == current_channel)
        return;

    // Reset the receive buffer, it's no longer valid
    spi_receive_circular_buffer.start = 0;
    spi_receive_circular_buffer.end = 0;
    current_channel = channel;

    // Set all slave outputs, we don't exit because
    // we want to set them all high except for channel
    SPI_SELECT_LED_BAR = 1;
    SPI_SELECT_TTS = 1;
    if (channel == LED_BAR)
        SPI_SELECT_LED_BAR = 0;
    if (channel == TTS)
        SPI_SELECT_TTS = 0;
}

void spi_TrasmitByte(const uint8_t data)
{
    SSPBUF = data;
    // Following line commented out because it caused trouble with debugging
    //while (PIR1bits.SSPIF == 0) { }
    circular_buffer_Write(&spi_receive_circular_buffer, &SSPBUF);
    PIR1bits.SSPIF = 0;
}

uint8_t spi_TrasmitData(const uint8_t* const data, const uint8_t bytes_to_send)
{
    uint8_t ii;
    for (ii = 0; ii < bytes_to_send; ii++)
    {
        spi_TrasmitByte(*(data+ii));
    }
    return ii;
}

void spi_GetReceiveByte(uint8_t* const data)
{
    // if we don't have a byte in the buffer, send nothing to get one
    if (circular_buffer_IsEmpty(&spi_receive_circular_buffer))
    {
        spi_TrasmitByte(0x00);
    }
    circular_buffer_Read(&spi_receive_circular_buffer, data);
}

uint8_t spi_GetReceiveData(uint8_t* const buffer, 
                           const uint8_t bytes_to_receive)
{
    uint8_t ii;
    for (ii = 0; ii < bytes_to_receive; ii++)
    {
        spi_GetReceiveByte(buffer + ii);
    }
    return ii;
}