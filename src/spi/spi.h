#ifndef __SPI_SPI_H__
#define __SPI_SPI_H__
/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          SPI Manager
    Author(s):      James Littlejohn

============================================================================= */
    
/** 
 *  @ingroup HARDWARE
 *  @defgroup SPI spi.h : SPI Manager
 *
 *  A manager for SPI module, to transparently talk to several devices.
 *
 *  Files: spi.h & spi.c
 *
 *  This module controls the sending and receiving of SPI data. SPI data is
 *  received whenever data is sent. As the protocol is quite fast, this is all
 *  done in a blocking manner, with everything received stored in a buffer until
 *  accessed.
 *  
 *  @{
 */

#include <stdint.h>

/**
 *  The possible SPI modules we can talk to.
 */
typedef enum { 
	NONE,     //**< All slaves disabled */
	LED_BAR,  //**< The strip of LEDs */ 
	TTS       //**< Test to speech */
} Channel_t;

/**
 *  Initialisation function. Sets up SPI hardware and variables.
 */
extern void spi_Init(void);

/**
 *  Transmits a byte to whatever device is currently selected.
 *
 *  @param data  The byte to be transmitted
 */
extern void spi_TrasmitByte(const uint8_t data);

/**
 *  Transmits an array of bytes to whatever device is currently selected.
 *
 *  @param[in] data           A pointer to the start of the array
 *  @param[in] bytes_to_send  Amount of bytes to send
 *
 *  @return uint8_t           Amount of bytes actually sent
 */
extern uint8_t spi_TrasmitData(const uint8_t* const data, 
	                           const uint8_t bytes_to_send);

/**
 *  Returns the first byte of the receive buffer for the current device.
 *
 *  @param[out] data  A pointer to the location to store the byte
 */
extern void spi_GetReceiveByte(uint8_t* const data);

/**
 *  Stores a stream of bytes from the receive buffer for the currently selected
 *  device.
 *
 *  @param[out] data              A pointer to the start of the array to store 
 *                                data
 *  @param[in]  bytes_to_receive  Amount of bytes to receive
 *
 *  @return uint8_t  Amount of bytes actually received
 */
extern uint8_t spi_GetReceiveData(uint8_t* const buffer, 
	                              const uint8_t bytes_to_receive);

/**
 *  Changes appropriate slave select pins, and resets the input buffer.
 *
 *  @param channel  Which device we're talking to now
 */
extern void spi_ChangeChannel(Channel_t channel);

/**
  *  @}
  */
#endif
