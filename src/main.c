/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          Main Program
    Author(s):      James Littlejohn

============================================================================= */
#include <xc.h>
#include <GenericTypeDefs.h>
#include <stdio.h>
#include <string.h>

#include "hardware_info.h"
#include "register_configs.h"

#include "spi/spi.h"
#include "scale/scale.h"
#include "uart/uart.h"
#include "lcd/lcd.h"
#include "led_bar/led_bar.h"
#include "tts/tts.h"
#include "buttons/buttons.h"

typedef enum { NOT_COUNTING, ENTER_COUNT, DISPLAY } CountMode_t;

// -- Strings!
char weight_str[17];
char lcd_line_1[17];
char lcd_line_2[17];
uint8_t recieved_number = 0;

// -- State stuff
BOOL stabilised = FALSE;
uint8_t count = 1;
BOOL count_prompted = FALSE;
Weight_t weight_units = GRAMS;
BOOL factory_mode = FALSE;
CountMode_t count_mode = NOT_COUNTING;
BOOL up_requested = FALSE;
BOOL down_requested = FALSE;
BOOL tare_requested = FALSE;

// -- Storing various weights
int24_t weight = 0;
int24_t last_weight = 0;
int24_t weight_raw = 0;

void ShowHelpPrompt()
{
    uart_TransmitString("Press 'h' for help menu\n\r");
}

void ShowHelpMenu()
{
    uart_TransmitString("Toggle Units \'u\'\n\r\
                         Toggle Count Mode\'c\'\n\r\
                         Tare \'t\'\n\r");
    if (factory_mode)
    {
        uart_TransmitString("Calibrate \'x\'\n\r\
                             Show Raw Weight Readings \'r\'\n\r\
                             Show Sample Statistics \'s\'\n\r\
                             Exit Factory Mode \'e\'\n\r");
    }
}

// ---- Functions that get called directly by the buttons
void Tare()
{
    tare_requested = TRUE;
}

void SwitchToFactoryMode()
{
    factory_mode = TRUE;
}

void CycleUnits()
{
    stabilised = FALSE;
    if (weight_units == GRAMS)
    {
        weight_units = OUNCES;
        uart_TransmitString("Units are in Ounces\r\n");
        ShowHelpPrompt();
    }
    else if (weight_units == OUNCES)
    {
        if (count_mode == DISPLAY)
        {
            weight_units = COUNT;
            uart_TransmitString("Showing item count\r\n");
            ShowHelpPrompt();
        }
        else
        {
            weight_units = GRAMS;
            uart_TransmitString("Units are in grams\r\n");
            ShowHelpPrompt();
        }
    }
    else
    {
        weight_units = GRAMS;
        uart_TransmitString("Units are in ounces\r\n");
        ShowHelpPrompt();
    }
}

void CycleCount()
{
    stabilised = FALSE;
    if (count_mode == ENTER_COUNT)
    {
        weight_units = COUNT;
        count_mode = DISPLAY;
    }
    else
    {
        count_mode = ENTER_COUNT;
    }
}

void Up()
{
    up_requested = TRUE;
}
void Down()
{
    down_requested = TRUE;
}

// -- Need to run all ProcessTasks functions here
void interrupt low_priority ISR_LOW(void)
{
    buttons_ProcessTasks();
    uart_ProcessTasks();
    scale_ProcessTasks();
}

// Run through the calibration procedure
void Calibrate()
{
    char input = 0;
    uart_TransmitString("Please place 100 grams in tray and press enter\n\r");
    while (input != '\n')
    {
        while (!uart_ReceiveBufferIsEmpty())
        {
            uart_GetReceiveByte(&input);
        }
    }
    uint24_t samp_100 = scale_GetWeight(RAW_NO_TARE);

    input = 0;
    uart_TransmitString("Please place 500 grams in tray and press enter\n\r");
    while (input != '\n')
    {
        while (!uart_ReceiveBufferIsEmpty())
        {
            uart_GetReceiveByte(&input);
        }
    }
    uint24_t samp_500 = scale_GetWeight(RAW_NO_TARE);

    uart_TransmitString("Saving calibration\n\r");
    scale_SaveCalibration(400.0/((double)samp_500-(double)samp_100));
    uart_TransmitString("Saved\n\r");
    ShowHelpPrompt();
}

void main_Init()
{
    // Make sure interrupts are turned off
    INTCONbits.GIE = 0;
    INTCONbits.PEIE = 0;

    // Init peripherals
    uart_Init();
    spi_Init();
    tts_Init();
    led_bar_Init();
    lcd_Init();
    scale_Init();

    // It's important this gets set before buttons starts
    buttons_FactoryMode = SwitchToFactoryMode;
    buttons_Init();

    // Enable interrupts
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
}

void main()
{
    main_Init();

    // Setup buttons
    buttons_TarePressed = Tare;
    buttons_UnitPressed = CycleUnits;
    buttons_ModePressed = CycleCount;
    buttons_UpPressed = Up;
    buttons_DownPressed = Down;

    uart_TransmitString("Welcome to NewtonSensor\n\r");
    ShowHelpPrompt();
    ShowHelpMenu();

    // Main Loop!
    while(TRUE)
    {
        // Always update the LED bar no matter what
        // The magic numbers in here are an attempt to get the thing working
        // full scale
        weight_raw = scale_GetWeight(RAW);
        led_bar_BarGraph((uint8_t)((weight_raw-86)*16.0/860.0), 0);
        
        // Deal with a tare request
        if (tare_requested)
        {
            scale_Tare();
            tare_requested = FALSE;
            uart_TransmitString("Scales Tared\r\n");
            ShowHelpPrompt();
        }

        // Display weight in the way you'd expect
        if (count_mode == NONE || count_mode == DISPLAY)
        {
            // -- DISPLAY WEIGHT
            weight = scale_GetWeight(weight_units);
            count_prompted = FALSE;
            sprintf(lcd_line_1, "%s", "   Weight Mode  ");
            if (weight_units == OUNCES)
            {
                sprintf(weight_str, "%7d Ounces", weight);
            }
            else if (weight_units == COUNT)
            {
                sprintf(weight_str, "%7d Items", weight);
            }
            else if (weight_units == GRAMS)
            {
                sprintf(weight_str, "%7d Grams", weight);
            }
            else
            {
                sprintf(weight_str, "%7d", weight);
            }
            sprintf(lcd_line_2, "%-16s", weight_str);

            // -- CHECK FOR STABILISATION
            if (weight == last_weight)
            {
                if (stabilised == FALSE)
                {
                    stabilised = TRUE;
                    tts_Speak(weight_str);
                    uart_TransmitString("Weight in scale: ");
                    uart_TransmitString(weight_str);
                    uart_TransmitString("\r\n");
                }
            }
            else if (weight > last_weight + 5 || weight + 5 < last_weight)
            {
                stabilised = FALSE;
            }
            last_weight = weight;
        }

        // Deal with count mode
        else if (count_mode == ENTER_COUNT)
        {
            if (count_prompted == FALSE)
            {
                tts_Speak("Enter items in tray then press count");
                uart_TransmitString("Enter amount of items in scale: \r\n");
                recieved_number = 0;
                count_prompted = TRUE;
            }
            sprintf(weight_str, "%d", count);
            if (up_requested)
            {
                count++;
                sprintf(weight_str, "%d", count);
                tts_Speak(weight_str);
                up_requested = FALSE;
            }
            else if (down_requested)
            {
                if (count > 1)
                    count--;
                sprintf(weight_str, "%d", count);
                tts_Speak(weight_str);
                down_requested = FALSE;
            }
            sprintf(lcd_line_1, "%s", "Count Mode Setup");
            sprintf(lcd_line_2, "%7d Items   ", count);
            scale_SetCount(count);
        }

        // Deal with serial events
        while(!uart_ReceiveBufferIsEmpty())
        {
            char data;
            uart_GetReceiveByte(&data);
            if (count_mode == ENTER_COUNT)
            {
                if (data == '\n')
                {
                    CycleCount();
                }
                else if (data >= '0' && data <= '9')
                {
                    // Convert to int
                    recieved_number = recieved_number*10 + (data - '0');
                    count = recieved_number;
                }
            }
            else
            {
                switch (data)
                {
                    case 'U':
                    case 'u':
                        CycleUnits();
                        break;
                    case 'H':
                    case 'h':
                        ShowHelpMenu();
                        break;
                    case 'C':
                    case 'c':
                        CycleCount();
                        break;
                    case 'T':
                    case 't':
                        Tare();
                        break;
                    default:
                        break;
                }
            }
            if (factory_mode)
            {
                switch (data)
                {
                    case 'X':
                    case 'x':
                        Calibrate();
                        break;
                    case 'R':
                    case 'r':
                        uart_TransmitString("Units are raw values\r\n");
                        weight_units = RAW_NO_TARE;
                        break;
                    case 'S':
                    case 's':
                        uart_TransmitString("Statistics unimplimented\r\n");
                        break;
                    case 'E':
                    case 'e':
                        uart_TransmitString("Exiting Factory Mode\n\r");
                        factory_mode = FALSE;
                        break;
                    default:
                        break;
                }
            }
        }

        // And then update the LCD, to whatever it should be doing
        lcd_Print(lcd_line_1, lcd_line_2);
    }
}