/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          LCD Manager
    Author(s):      James Littlejohn

============================================================================= */
#include <xc.h>
#include <GenericTypeDefs.h>
#include <stdint.h>
#include "hardware_info.h"

#include "lcd/lcd.h"

// Writes half a byte to the LCD
void lcd_WriteNibble(uint8_t nibble, BOOL command)
{
    const uint8_t rs_mask = command == FALSE ? LCD_RS_MASK : 0;
    LCD_PORT = nibble | LCD_EN_MASK | rs_mask;
    LCD_PORT = nibble | rs_mask;
}

// Writes a byte, using 2 nibble writes.
void lcd_WriteByte(uint8_t cmd, BOOL command)
{
    lcd_WriteNibble(cmd >> 4, command);
    lcd_WriteNibble(cmd & 0x0F, command);
    
    // Let the LCD process this
    __delay_us(400);
}

void lcd_Init(void)
{
    // We can only start this init 400 mseconds after the LCD is powered
    for (uint8_t ii = 0; ii < 40; ii++, __delay_ms(10));
    
    // Set ports
    LCD_PORT_TRIS = 0x00;

    // Reset LCD
    LCD_PORT = 0xFF;
    __delay_ms(20);
    lcd_WriteNibble(0x3, TRUE);
    __delay_ms(10);
    lcd_WriteNibble(0x3, TRUE);
    __delay_ms(1);
    lcd_WriteNibble(0x3, TRUE);
    __delay_ms(1);
    // 4 bits mode
    lcd_WriteNibble(0x2, TRUE);
    __delay_ms(1);

    // Set up parameters
    lcd_WriteByte(0x28, TRUE); // 4-bit mode - 2 line - 5x7 font.
	lcd_WriteByte(0x0C, TRUE); // Display no cursor - no blink.
	lcd_WriteByte(0x06, TRUE); // Automatic Increment - No Display shift.
    lcd_WriteByte(0x01, TRUE); // Clear display

    // Give this all a bit to sink in.
    // The fundamental rule of mechatronics is add delays until it works.
    for (uint8_t ii = 0; ii < 10; ii++, __delay_ms(10));
}

// TODO: Have this function find \0 and just print spaces when found
void lcd_Print(const char* line_1, const char* line_2)
{
    uint8_t ii;

    // Place cursor at line 1
    lcd_WriteByte(0x80, TRUE);
    for (ii = 0; ii < 16; ii++)
    {
        lcd_WriteByte(*line_1++, FALSE);
    }

    // Place cursor at line 2
    lcd_WriteByte(0x80 + 0x40, TRUE); 
    for (ii = 0; ii < 16; ii++)
    {
        lcd_WriteByte(*line_2++, FALSE);
    }
}