#ifndef __LCD_LCD_H__
#define __LCD_LCD_H__
/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          LCD Manager
    Author(s):      James Littlejohn

============================================================================= */

/** 
 *  @ingroup HARDWARE
 *  @defgroup SCALE lcd.h : LCD Manager
 *
 *  A manager for LCD hardware.
 *
 *  Files: lcd.h & lcd.c
 *
 *  This module will setup and manage the LCD hardware. We're currently running
 *  it in 4 bits mode.
 *  
 *  @{
 */

#include <GenericTypeDefs.h>
#include <stdint.h>

/**
 *  Initialisation function. Make sure the timing is correct for the LCD
 *  hardware, and then send it the correct startup code.
 */
extern void lcd_Init(void);

/**
 *  Prints strings to the LCD hardware. Both inputs need to be 16 characters
 *  long (17 including null), or you'll end up printing nonsense.
 *  
 *  @param[in] line_1  First line of text
 *  @param[in] line_2  Second line of text
 */
extern void lcd_Print(const char* line_1, const char* line_2);

/**
  *  @}
  */
#endif
