/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          UART Manager
    Author(s):      James Littlejohn

============================================================================= */
#include <xc.h>
#include <GenericTypeDefs.h>
#include <stdint.h>
#include "hardware_info.h"
#include "circular_buffer/circular_buffer.h"

#include "uart/uart.h"

#define UART_TRANSMIT_BUFFER_SIZE 128
#define UART_RECEIVE_BUFFER_SIZE 8

static BOOL             initialised = FALSE;
static CircularBuffer_t uart_receive_circular_buffer;
static uint8_t          uart_receive_buffer[UART_RECEIVE_BUFFER_SIZE];
static CircularBuffer_t uart_transmit_circular_buffer;
static uint8_t          uart_transmit_buffer[UART_TRANSMIT_BUFFER_SIZE];

void uart_Init(void)
{
    if (initialised)
        return;
    
    circular_buffer_Init(&uart_receive_circular_buffer, uart_receive_buffer, 
        UART_RECEIVE_BUFFER_SIZE, 
        sizeof(uart_receive_buffer)/UART_RECEIVE_BUFFER_SIZE);
    circular_buffer_Init(&uart_transmit_circular_buffer, uart_transmit_buffer, 
        UART_TRANSMIT_BUFFER_SIZE, 
        sizeof(uart_transmit_buffer)/UART_TRANSMIT_BUFFER_SIZE);

    // Setup pins
    UART_CLK_TRIS = 0;
    UART_RX_TRIS = 1;

    // UART setup
    TXSTAbits.TX9  = 0; // 8 Bit transmission mode
    TXSTAbits.TXEN = 0; // Transmit disabled (gets conditionally enabled)
    TXSTAbits.SYNC = 0; // Async mode
    RCSTAbits.RX9  = 0; // 8 Bit receive mode
    RCSTAbits.CREN = 1; // Enable receiver

    // Baud rate is 9600 bps
    TXSTAbits.BRGH = 1; // High Baud Rate
    SPBRG = 64;         // 9600 bps

    // Enable Interrupts
    PIE1bits.TXIE = 1; // Transmit interrupt enabled
    PIE1bits.RCIE = 1; // Receive interrupt enabled
    
    // Turn it on
    RCSTAbits.SPEN = 1; // Serial port enable

    initialised = TRUE;
}

void uart_ProcessTasks(void)
{
    if (!initialised)
        return;

    uint8_t data;
    // Interrupt stuff, empty and fill buffers
    if (PIR1bits.TXIF == 1)
    {
        circular_buffer_Read(&uart_transmit_circular_buffer, &data);
        TXREG = data;
        if (uart_TransmitBufferIsEmpty())
        {
            TXSTAbits.TXEN = 0;
        }
    }
    if (PIR1bits.RCIF == 1)
    {
        uint8_t data = RCREG;
        circular_buffer_Write(&uart_receive_circular_buffer, &data);
    }
}

BOOL uart_ReceiveBufferIsFull(void)
{
    return circular_buffer_IsFull(&uart_receive_circular_buffer);
}

BOOL uart_ReceiveBufferIsEmpty(void)
{
    return circular_buffer_IsEmpty(&uart_receive_circular_buffer);
}

void uart_GetReceiveByte(void* const data)
{
    circular_buffer_Read(&uart_receive_circular_buffer, data);
}

uint8_t uart_GetReceiveData(void* const buffer, const uint8_t bytes_to_receive)
{
    return circular_buffer_ReadData(&uart_receive_circular_buffer, 
                                    buffer, bytes_to_receive);
}

BOOL uart_TransmitBufferIsFull(void)
{
    return circular_buffer_IsFull(&uart_transmit_circular_buffer);
}

BOOL uart_TransmitBufferIsEmpty(void)
{
    return circular_buffer_IsEmpty(&uart_transmit_circular_buffer);
}

void uart_TransmitByte(const void* const data)
{
    circular_buffer_Write(&uart_transmit_circular_buffer, data);
    // And as we definitely have data, enable the transmit interrupt
   TXSTAbits.TXEN = 1;
}

uint8_t uart_TransmitData(const void* const data, const uint8_t bytes_to_send)
{
    uint8_t ret_val = circular_buffer_WriteData(&uart_transmit_circular_buffer, 
                                                data, bytes_to_send);
    // Make sure we actually filled this with some data, before getting excited
    if (ret_val > 0)
    {
        TXSTAbits.TXEN = 1;
    }
    return ret_val;
}

uint8_t uart_TransmitString(const char* data)
{
    uint8_t ret_val;
    for (ret_val = 0; *data != '\0'; ret_val++, data++)
    {
        circular_buffer_Write(&uart_transmit_circular_buffer, data);
    }
    if (ret_val > 0)
    {
        TXSTAbits.TXEN = 1;
    }
    return ret_val;
}