#ifndef __UART_UART_H__
#define __UART_UART_H__
/* =============================================================================
        
    Copyright (C) 2013  James Littlejohn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Title:          UART Manager
    Author(s):      James Littlejohn

============================================================================= */
    
/** 
 *  @ingroup HARDWARE
 *  @defgroup UART uart.h : UART Manager
 *
 *  A manager for serial communications.
 *
 *  Files: uart.h & uart.c
 *
 *  This module will setup everything needed for management of serial. This
 *  module manages all data in internal buffers (send/receive) and manipulates
 *  the interrupts in order to send/receive them.
 *
 *  The module will use the receive interrupt to fill the receive buffer, and
 *  when the module detects data in the send buffer, it will enable the send
 *  interrupt until the buffer is empty.
 *
 *  @{
 */

#include <stdint.h>

/**
 *  Initialisation function. Sets up serial system.
 */
extern void uart_ProcessTasks(void);

/**
 *  The interrupt function of this module. It needs to be called every time an
 *  interrupt is detected. If the interrupt is relevant to this module, it will
 *  deal with it at reset the appropriate flag.
 */
extern void uart_Init(void);

/**
 *  See if the receive buffer is full.
 *
 *  @retval TRUE    Buffer is full
 *  @retval FALSE   Buffer is NOT full
 */
extern BOOL uart_ReceiveBufferIsFull(void);

/**
 *  See if the receive buffer is empty.
 *
 *  @param cb      Pointer to the circular buffer object
 *
 *  @retval TRUE   Buffer is empty
 *  @retval FALSE  Buffer contains data
 */
extern BOOL uart_ReceiveBufferIsEmpty(void);

/**
 *  See if the transmit buffer is full.
 *
 *  @retval TRUE    Buffer is full
 *  @retval FALSE   Buffer is NOT full
 */
extern BOOL uart_TransmitBufferIsFull(void);

/**
 *  See if the transmit buffer is empty.
 *
 *  @param cb      Pointer to the circular buffer object
 *
 *  @retval TRUE   Buffer is empty
 *  @retval FALSE  Buffer contains data
 */
extern BOOL uart_TransmitBufferIsEmpty(void);

/**
 *  Places the first element of the receive data buffer in data.
 *
 *  @param[out data  Where to place the data
 */
extern void uart_GetReceiveByte(void* const data);

/**
 *  Read (retrieve) data from the receive buffer.
 *
 *  @param buffer            Pointer location where data will be stored
 *  @param bytes_to_receive  Amount of data bytes to read
 */
extern uint8_t uart_GetReceiveData(void* const buffer, 
	                               const uint8_t bytes_to_receive);

/**
 *  Transmits a byte of data.
 *
 *  @param[out data  Pointer to the location of the data
 */
extern void uart_TransmitByte(const void* const data);

/**
 *  Transmit data.
 *
 *  @param data            Pointer location where data is stored
 *  @param bytes_to_send   Amount of data bytes to send
 */
extern uint8_t uart_TransmitData(const void* const data, 
	                             const uint8_t bytes_to_send);

/**
 *  Transmit a null terminated string over serial.
 *
 *  @param data            Pointer to the string
 */
extern uint8_t uart_TransmitString(const char* data);

/**
  *  @}
  */
#endif
