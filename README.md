# Computer Controlled Scales
This is the git repository for the computer controlled scales. This was developed as part of the major project for MTRX3700.

## Project Layout
* **/ide** is where the MPLABX project resides
* **/src** is where all source code resides
* **/src/module_name** contains 3 files
* **/src/module_name/module_name.h** The main header for the module
* **/src/module_name/module_name.c** The implimentation of the module